package informationSystem;

import java.awt.BorderLayout;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static informationSystem.Connection.getConnection;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class RealizarPesquisa {

    public static void visualizarEquipa() throws Exception {

        String use1 = JOptionPane.showInputDialog(null, "Escreva o id da Equipa:\n");

        java.sql.Connection con = getConnection();
        PreparedStatement obter = con.prepareStatement("SELECT* FROM Equipa WHERE equipa_id = '" + use1 + "' limit 1");
        ResultSet elemento = obter.executeQuery();

        ResultSetMetaData rsmt = elemento.getMetaData();
        int c = rsmt.getColumnCount();
        Vector column = new Vector(c);
        for (int i = 1; i <= c; i++) {
            column.add(rsmt.getColumnName(i));
        }
        Vector data = new Vector();
        Vector row = new Vector();
        while (elemento.next()) {
            row = new Vector(c);
            for (int i = 1; i <= c; i++) {
                row.add(elemento.getString(i));
            }
            data.add(row);
        }
        JFrame frame = new JFrame();
        frame.setSize(500, 120);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JTable table = new JTable(data, column);
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp, BorderLayout.CENTER);
        frame.setContentPane(panel);
        frame.setVisible(true);

    }

    static void Visualizar(String id, String tabela, String coluna) throws Exception {

        java.sql.Connection con = getConnection();
        PreparedStatement obter = con.prepareStatement("SELECT* FROM " + tabela + " WHERE " + coluna + " = '" + id + "'");
        ResultSet elemento = obter.executeQuery();

        ResultSetMetaData rsmt = elemento.getMetaData();
        int c = rsmt.getColumnCount();
        Vector column = new Vector(c);
        for (int i = 1; i <= c; i++) {
            column.add(rsmt.getColumnName(i));
        }
        Vector data = new Vector();
        Vector row = new Vector();
        while (elemento.next()) {
            row = new Vector(c);
            for (int i = 1; i <= c; i++) {
                row.add(elemento.getString(i));
            }
            data.add(row);
        }
        JFrame frame = new JFrame();
        frame.setSize(500, 120);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JTable table = new JTable(data, column);
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp, BorderLayout.CENTER);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    static void visualizarProvas() throws Exception {

        String use1 = JOptionPane.showInputDialog(null, "Escreva o id da prova:\n");

        java.sql.Connection con = getConnection();
        PreparedStatement obter = con.prepareStatement("SELECT* FROM Provas WHERE prova_id = '" + use1 + "' limit 1");
        ResultSet elemento = obter.executeQuery();

        ResultSetMetaData rsmt = elemento.getMetaData();
        int c = rsmt.getColumnCount();
        Vector column = new Vector(c);
        for (int i = 1; i <= c; i++) {
            column.add(rsmt.getColumnName(i));
        }
        Vector data = new Vector();
        Vector row = new Vector();
        while (elemento.next()) {
            row = new Vector(c);
            for (int i = 1; i <= c; i++) {
                row.add(elemento.getString(i));
            }
            data.add(row);
        }
        JFrame frame = new JFrame();
        frame.setSize(500, 120);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JTable table = new JTable(data, column);
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp, BorderLayout.CENTER);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }
}
