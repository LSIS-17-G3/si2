package informationSystem;

import javax.swing.JOptionPane;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Main {

    public static void main(String[] args) throws Exception {

        String message = null;
        String message1 = null;
        String message2 = null;
        String a = null;
        

        do {
            a = JOptionPane.showInputDialog(null, "Escolha uma opção:\n 1) Equipas\n 2) Material\n 3) Provas\n 4) Robo\n 5) Utilizador\n 0) Sair").toUpperCase();

            if ("1".equals(a)) {
                message1 = JOptionPane.showInputDialog(null, " 1) Informacao Equipa\n 2) Inserir Equipa\n 3) Eliminar Equipa\n 4) Voltar ao Menu Principal ").toUpperCase();
                if ("1".equals(message1)) {
                    message2 = JOptionPane.showInputDialog(null, " 1) Listar Equipa\n 2) Visualizar Equipa\n 3) Atualizar Informação\n 4) Voltar ao Menu Principal ").toUpperCase();
                    if ("1".equals(message2)) {
                        ObterTabela.obterTabela("Equipa");
                    } else if ("2".equals(message2)) {
                        String id = JOptionPane.showInputDialog(null, "Escreva o Id da Equipa:\n");
                        RealizarPesquisa.Visualizar(id, "Equipa", "equipa_id");
                    } else if ("3".equals(message2)) {
                        AlterarTabela.atualizarEquipa();
                    }
                } else if ("2".equals(message1)) {
                    InserirDados.inserirEquipa();

                } else if ("3".equals(message1)) {
                    AlterarTabela.EliminarEquipa();

                }
            } else if ("2".equals(a)) {
                message1 = JOptionPane.showInputDialog(null, " 1) Informacao Material\n 2) Inserir Material\n 3) Eliminar Material\n 4) Voltar ao Menu Principal ").toUpperCase();
                if ("1".equals(message1)) {
                    message2 = JOptionPane.showInputDialog(null, " 1) Listar Materiais\n 2) Visualizar Material\n 3) Atualizar Material\n 4) Voltar ao Menu Principal ").toUpperCase();
                    if ("1".equals(message2)) {
                        ObterTabela.obterTabela("Material");
                    } else if ("2".equals(message2)) {
                        String id = JOptionPane.showInputDialog(null, "Escreva o Id do Robo Associado ao Material:\n");
                        RealizarPesquisa.Visualizar(id, "Material", "id_robot");
                    } else if ("3".equals(message2)) {
                        AlterarTabela.atualizarMaterial();
                    }
                } else if ("2".equals(message1)) {
                    InserirDados.inserirMaterial();

                } else if ("3".equals(message1)) {
                    AlterarTabela.EliminarMaterial();

                }
            } else if ("3".equals(a)) {
                message1 = JOptionPane.showInputDialog(null, " 1) Informacao Provas\n 2) Inserir Provas\n 3) Eliminar Provas\n 4) Voltar ao Menu Principal ").toUpperCase();
                if ("1".equals(message1)) {
                    message2 = JOptionPane.showInputDialog(null, " 1) Listar Provas\n 2) Visualizar Prova\n 3) Atualizar Provas\n 4) Voltar ao Menu Principal ").toUpperCase();
                    if ("1".equals(message2)) {
                        ObterTabela.obterTabela("Provas");
                    } else if ("2".equals(message2)) {
                        String id = JOptionPane.showInputDialog(null, "Escreva o Id da Prova:\n");
                        RealizarPesquisa.Visualizar(id, "Provas", "prova_id");
                    } else if ("3".equals(message2)) {
                        AlterarTabela.atualizarProvas();
                    }
                } else if ("2".equals(message1)) {
                    InserirDados.inserirProvas();

                } else if ("3".equals(message1)) {
                    AlterarTabela.EliminarProvas();

                }
            } else if ("4".equals(a)) {
                message1 = JOptionPane.showInputDialog(null, " 1) Informacao Robo\n 2) Inserir Robo\n 3) Eliminar Robo\n 4) Voltar ao Menu Principal ").toUpperCase();
                if ("1".equals(message1)) {
                    message2 = JOptionPane.showInputDialog(null, " 1) Listar Robos\n 2) Visualizar Robo\n 3) Atualizar Robo\n 4) Voltar ao Menu Principal ").toUpperCase();
                    if ("1".equals(message2)) {
                        ObterTabela.obterTabela("Robot");
                    } else if ("2".equals(message2)) {
                        String id = JOptionPane.showInputDialog(null, "Escreva o Id do Robo:\n");
                        RealizarPesquisa.Visualizar(id, "Robot", "id_robot");
                    } else if ("3".equals(message2)) {
                        AlterarTabela.atualizarRobo();
                    }
                } else if ("2".equals(message1)) {
                    InserirDados.inserirRobo();

                } else if ("3".equals(message1)) {
                    AlterarTabela.EliminarRobo();
                }
            } else if ("5".equals(a)) {
                message1 = JOptionPane.showInputDialog(null, " 1) Informacao Utilizador\n 2) Inserir Utilizador\n 3) Eliminar Utilizador\n 4) Voltar ao Menu Principal").toUpperCase();
                if ("1".equals(message1)) {
                    message2 = JOptionPane.showInputDialog(null, " 1) Listar Utilizadores\n 2) Visualizar Utilizador\n 3) Atualizar Utilizador\n 4) Voltar ao Menu Principal ").toUpperCase();
                    if ("1".equals(message2)) {
                        ObterTabela.obterTabela("Utilizador");
                    } else if ("2".equals(message2)) {
                        String id = JOptionPane.showInputDialog(null, "Escreva o Id do Utilizador:\n");
                        RealizarPesquisa.Visualizar(id, "Utilizador", "id");
                    } else if ("3".equals(message2)) {
                        AlterarTabela.atualizarUtilizador();
                    }
                } else if ("2".equals(message1)) {
                    InserirDados.inserirUtilizador();

                } else if ("3".equals(message1)) {
                    AlterarTabela.EliminarUtilizador();

                }
            }

        } while (!"0".equals(a) && !"N".equals(message));

    }
}
