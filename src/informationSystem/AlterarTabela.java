package informationSystem;

import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import static informationSystem.Connection.getConnection;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class AlterarTabela {

    public static void atualizarEquipa() throws Exception {
        String use1 = JOptionPane.showInputDialog(null, "Insira o id da equipa que pretende alterar:");
        String use2 = JOptionPane.showInputDialog(null, "O que pretende alterar?\n 1) Nome da Equipa\n 2) Número de elementos\n 3) Localidade");
        if ("1".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo nome da Equipa:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET nome = '" + use3 + "' WHERE equipa_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if ("2".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo número de elementos");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET num_elementos = '" + use3 + "' WHERE equipa_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("3".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova localidade:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET localidade = '" + use3 + "' WHERE equipa_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    static void atualizarMaterial() throws Exception {
        String use1 = JOptionPane.showInputDialog(null, "Insira o id do robot que contém o material:");
        String use2 = JOptionPane.showInputDialog(null, "Insira o nome do material que pretende alterar:");
        String use3 = JOptionPane.showInputDialog(null, "Introduza o novo nome do material:");

        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("UPDATE Material SET nome_material = '" + use3 + "' WHERE id_robot = '" + use1 + "' AND nome_material = '" + use2 + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    static void atualizarProvas() {

        String use1 = JOptionPane.showInputDialog(null, "Insira o id da prova que pretende alterar:");
        String use2 = JOptionPane.showInputDialog(null, "O que pretende alterar?\n 1) Categoria\n 2) Data\n 3) Hora\n 4) Tempo da Prova\n 5) Ranking\n 6) Local da Prova\n 7) Voltar ao menu principal");

        if ("1".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova categoria:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET categoria = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if ("2".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova data da prova no formato dd/mm/yyyy:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET data = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("3".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova hora da prova:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET hora = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("4".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo tempo da prova:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET tempo_prova = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("5".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo ranking");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET ranking = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("6".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo local da prova:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Equipa SET local_prova = '" + use3 + "' WHERE prova_id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }

    }

    static void atualizarRobo() throws Exception {
        String use1 = JOptionPane.showInputDialog(null, "Insira o id do robo que pretende alterar:");
        String use2 = JOptionPane.showInputDialog(null, "O que pretende alterar?\n 1) Id da Equipa\n 2) Cor\n 3) Dimensões\n 4) Voltar ao menu principal");

        if ("1".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo id da equipa:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Robot SET equipa_id = '" + use3 + "' WHERE id_robot = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if ("2".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova cor:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Robot SET cor = '" + use3 + "' WHERE id_robot = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if ("3".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza as novas dimensões:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Robot SET dimensoes = '" + use3 + "' WHERE id_robot = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }
    }

    static void atualizarUtilizador() {

        String use1 = JOptionPane.showInputDialog(null, "Insira o id do Utilizador que pretende alterar");
        String use2 = JOptionPane.showInputDialog(null, "O que pretende alterar?\n 1) Id da Equipa\n 2) Nome\n 3) Idade\n 4) Data de Nascimento\n 5) Voltar ao menu principal");

        if ("1".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo id da equipa:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Utilizador SET equipa_id = '" + use3 + "' WHERE id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        } else if ("2".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza o novo nome:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Utilizador SET nome = '" + use3 + "' WHERE id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("3".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova idade:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Utilizador SET idade = '" + use3 + "' WHERE id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else if ("4".equals(use2)) {
            String use3 = JOptionPane.showInputDialog(null, "Introduza a nova data de nascimento:");
            try {
                java.sql.Connection con = getConnection();
                PreparedStatement insert = con.prepareStatement("UPDATE Utilizador SET data_nascimento = '" + use3 + "' WHERE id = '" + use1 + "'");
                insert.executeUpdate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    static void EliminarEquipa() {
        String id = JOptionPane.showInputDialog(null, "Indique o id da equipa a ser apagada:");
        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("DELETE from Equipa where equipa_id = '" + id + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void EliminarMaterial() {
        String id = JOptionPane.showInputDialog(null, "Indique o id do robo associado ao material:");
        String nome = JOptionPane.showInputDialog(null, "Indique o nome do material a ser eliminado:");

        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("DELETE from Material where id_robot = '" + id + "' and nome_material = '" + nome + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void EliminarProvas() {
        String id = JOptionPane.showInputDialog(null, "Indique o id da prova");
        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("DELETE from Provas where prova_id = '" + id + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void EliminarRobo() {
        String id = JOptionPane.showInputDialog(null, "Indique o id do robo");
        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("DELETE from Robot where id_robot = '" + id + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void EliminarUtilizador() {
        String id = JOptionPane.showInputDialog(null, "Indique o id do Utilizador");
        try {
            java.sql.Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("DELETE from Utilizador where id = '" + id + "'");
            insert.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
}
