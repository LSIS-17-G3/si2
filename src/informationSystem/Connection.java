package informationSystem;

import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class Connection {

    public static java.sql.Connection getConnection() throws Exception {
        try {
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/roboBombeiro";
            String username = "henriquemelo";
            String password = "henrique123";
            Class.forName(driver).newInstance();
            java.sql.Connection conn = DriverManager.getConnection(url, username, password);
            return conn;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return null;
    }

}
