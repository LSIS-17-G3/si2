package informationSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import static informationSystem.Connection.getConnection;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class InserirDados {

    public static void inserirEquipa() {
        String nomeEquipa = JOptionPane.showInputDialog(null, "Insira o nome da equipa:");
        String numElementos = JOptionPane.showInputDialog(null, "Insira o número de elementos:");
        String localidade = JOptionPane.showInputDialog(null, "Insira a Localidade:");
        try {
            Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("INSERT INTO Equipa(nome,num_elementos,localidade) VALUES ('" + nomeEquipa + "','" + numElementos + "','" + localidade + "')");
            insert.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void inserirMaterial() {
        String idRobo = JOptionPane.showInputDialog(null, "Insira o id do robo associado a este material:");
        String nomeMat = JOptionPane.showInputDialog(null, "Insira o nome do material:");
        try {
            Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("INSERT INTO Material(id_robot,nome_material) VALUES ('" + idRobo + "','" + nomeMat + "')");
            insert.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void inserirProvas() {
        String idEquipa = JOptionPane.showInputDialog(null, "Insira o id da equipa:");
        String categoria = JOptionPane.showInputDialog(null, "Insira a categoria da prova:");
        String data = JOptionPane.showInputDialog(null, "Insira a data da prova no formato dd/mm/yyyy:");
        String hora = JOptionPane.showInputDialog(null, "Insira a hora da prova:");
        String tempoProva = JOptionPane.showInputDialog(null, "Insira o tempo da prova:");
        String ranking = JOptionPane.showInputDialog(null, "Insira o ranking da prova:");
        String localProva = JOptionPane.showInputDialog(null, "Insira o local da prova:");
        try {
            Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("INSERT INTO Provas(equipa_id,categoria,data,hora,tempo_prova,ranking,local_prova) VALUES ('" + idEquipa + "','" + categoria + "','" + data + "','" + hora + "','" + tempoProva + "','" + ranking + "','" + localProva + "')");
            insert.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void inserirRobo() {
        String idEquipa = JOptionPane.showInputDialog(null, "Insira o id da equipa associada ao robo:");
        String cor = JOptionPane.showInputDialog(null, "Insira a cor do robo:");
        String dimensoes = JOptionPane.showInputDialog(null, "Insira as dimensões:");
        try {
            Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("INSERT INTO Robot(equipa_id,cor,dimensoes) VALUES ('" + idEquipa + "','" + cor + "','" + dimensoes + "')");
            insert.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    static void inserirUtilizador() {
        String idEquipa = JOptionPane.showInputDialog(null, "Insira o id da equipa associada ao utilizador:");
        String nome = JOptionPane.showInputDialog(null, "Insira o nome do utilizador");
        String idade = JOptionPane.showInputDialog(null, "Insira a idade do utilizador:");
        String dataNasc = JOptionPane.showInputDialog(null, "Insira a data de nascimento do utilizador no formato dd/mm/yyyy:");
        try {
            Connection con = getConnection();
            PreparedStatement insert = con.prepareStatement("INSERT INTO Utilizador(equipa_id,nome,idade,data_nascimento) VALUES ('" + idEquipa + "','" + nome + "','" + idade + "','" + dataNasc + "')");
            insert.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

}
