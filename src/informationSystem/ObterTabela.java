package informationSystem;

import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static informationSystem.Connection.getConnection;

/**
 *
 * @author Henrique Melo <1150494@isep.ipp.pt>
 */
public class ObterTabela {

    public static void obterTabela(String use) throws Exception {

        try {

            Connection con = getConnection();
            PreparedStatement obter = con.prepareStatement("SELECT* FROM " + use);
            ResultSet tabela = obter.executeQuery();

            ResultSetMetaData rsmt = tabela.getMetaData();
            int c = rsmt.getColumnCount();
            Vector column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                column.add(rsmt.getColumnName(i));
            }
            Vector data = new Vector();
            Vector row = new Vector();
            while (tabela.next()) {
                row = new Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(tabela.getString(i));
                }
                data.add(row);
            }
            JFrame frame = new JFrame();
            frame.setSize(500, 300);
            frame.setLocationRelativeTo(null);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JPanel panel = new JPanel();
            JTable table = new JTable(data, column);
            JScrollPane jsp = new JScrollPane(table);
            panel.setLayout(new BorderLayout());
            panel.add(jsp, BorderLayout.CENTER);
            frame.setContentPane(panel);
            frame.setVisible(true);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR");
        }
    }

}
